//1 fruits on sale
db.fruits.aggregate([
        {
            $match: { onSale: true }
        },
	{ $count: "fruitsonSale"}
]);

//2 stocks over 20
db.fruits.aggregate([
        {
           $match: { stock : { $gte : 20} }
        },
	{ $count: "StocksOver20"}
]);

//3 avg price 
db.fruits.aggregate([
        {
           $match: { onSale: true }
        },
	{ 
        $group: {
			_id: "$supplier_id",
            price: { $avg: "$price"}
		}
    }
]);
//4 highest price fruit per supplier
db.fruits.aggregate([
        {
           $match: { onSale: true }
        },
	{ 
        $group: {
			_id: "$supplier_id",
            price: { $max: "$price"}
		}
    }
]);
//5 lowest price fruit per supplier
db.fruits.aggregate([
        {
           $match: { onSale: true }
        },
	{ 
        $group: {
			_id: "$supplier_id",
            price: { $min: "$price"}
		}
    }
]);